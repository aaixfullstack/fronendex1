import { ref, reactive } from 'vue'
import { defineStore } from 'pinia'
import axios from 'axios'

export const useUser2Store = defineStore('User2', () => {
  const editedUser = reactive({
    id: -1,
    email: '',
    password: '',
    age: '',
    name: ''
  })
  const users = ref([])
  async function deleteUser(user) {
    try {
      await axios.delete('http://127.0.0.1:3000/users/' + user.id)
      await getUsers()
    } catch (e) {
      console.log('ERROR')
    }
  }
  async function saveUser() {
    if (editedUser.id > 0) {
      try {
        await axios.patch('http://127.0.0.1:3000/users/' + editedUser.id, editedUser)
        await getUsers()
      } catch (e) {
        console.log('ERROR')
      }
    } else {
      try {
        await axios.post('http://127.0.0.1:3000/users', editedUser)
        await getUsers()
      } catch (e) {
        console.log('ERROR' + e)
      }
    }
    clearEditedUser()
  }
  function clearEditedUser() {
    editedUser.id = -1
    editedUser.email = ''
    editedUser.password = ''
    editedUser.age = ''
    editedUser.name = ''
  }
  async function getUsers() {
    const res = await axios.get('http://127.0.0.1:3000/users')
    console.log(res.data)
    users.value = res.data
  }
  return { users, deleteUser, saveUser, editedUser, clearEditedUser, getUsers }
})
