import { ref, reactive } from 'vue'
import { defineStore } from 'pinia'

export const useUserStore = defineStore('User', () => {
  let lastId = 3
  const editedUser = reactive({
    id: -1,
    email: '',
    password: '',
    age: '',
    name: ''
  })
  const users = ref([
    { id: 1, email: 'user1@mail.com', password: 'Password@1234', age: 20, name: 'User 1' },
    { id: 2, email: 'user2@mail.com', password: 'Password@1234', age: 22, name: 'User 2' }
  ])
  function deleteUser(user) {
    const index = users.value.findIndex((item) => user === item)
    users.value.splice(index, 1)
  }
  function saveUser() {
    if (editedUser.id > 0) {
      const index = users.value.findIndex((item) => item.id === editedUser.id)
      users.value[index] = JSON.parse(JSON.stringify(editedUser))
    } else {
      editedUser.id = lastId++
      users.value.push(JSON.parse(JSON.stringify(editedUser)))
    }
    clearEditedUser()
  }
  function clearEditedUser() {
    editedUser.id = -1
    editedUser.email = ''
    editedUser.password = ''
    editedUser.age = ''
    editedUser.name = ''
  }
  return { users, deleteUser, saveUser, editedUser, clearEditedUser }
})
